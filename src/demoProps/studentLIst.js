import React, { Component } from "react";
import StudentItem from "./studentItem";
import "./studentList.css";
class StudentList extends Component {
  //   fullName = "anh kiet";
  //   age = 13;
  students = [
    {
      id: 1,
      name: "hieu",
      age: 12,
    },
    {
      id: 2,
      name: "kiet",
      age: 16,
    },
  ];

  renderStudent = () => {
    const studentUI = this.students.map((item) => {
      return (
        <StudentItem
          key={item.id}
          student={item}
          setSelectedStudent={this.setSelectedStudent}
        />
      );
    });
    return studentUI;
  };

  selectedStudent = "";

  state = {
    selectedStudent: "",
  };

  setSelectedStudent = (val) => {
    this.selectedStudent = val;
    console.log(this.selectedStudent);

    this.setState({
      selectedStudent: val,
    });
    
  };
  render() {
    return (
      <div className="list">
        <h1>Student List</h1>
        <p>Selected Student : {this.state.selectedStudent}</p>
        {/* <StudentItem name={this.fullName} age={this.age} /> */}
        {/* <StudentItem student={this.students[0]} />
        <StudentItem student={this.students[1]} /> */}
        {this.renderStudent()}
      </div>
    );
  }
}

export default StudentList;
