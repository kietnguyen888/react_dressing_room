import React, { Component } from "react";
import Cart from "./cart";
import Detail from "./detail";
import ProductList from "./productList";

class Home extends Component {
  state = {
    selectedProduct: null,
    cart: [],
  };

  setSelectedProduct = (val) => {
    this.setState({
      selectedProduct: val,
    });
  };

  addToCart = (prod) => {
    const cloneCart = [...this.state.cart];

    //kiem tra sp ton tai trong gio hang chua
    //neu chua ,push vao cartItem
    //neu co thi tang quantity

    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === prod.id;
    });

    if (foughtIndex === -1) {
      const cartItem = { product: prod, quantity: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[foughtIndex].quantity++;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  increaseQuantity = (id) => {
    const cloneCart = [...this.state.cart];

    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });

    cloneCart[foughtIndex].quantity++;

    this.setState({
      cart: cloneCart,
    });
  };

  decreaseQuantity = (id) => {
    const cloneCart = [...this.state.cart];

    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });

    cloneCart[foughtIndex].quantity--;

    if (!cloneCart[foughtIndex].quantity) {
      this.deleteFromCart(id);
      return;
    }
    this.setState({
      cart: cloneCart,
    });
  };

  handlePayment = () => {
    this.setState({
      cart: [],
    });
  };

  deleteFromCart = (id) => {
    const cloneCart = [...this.state.cart];
    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });
    cloneCart.splice(foughtIndex, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  products = [
    {
      id: "sp_1",
      name: "iphoneX",
      price: 3000,
      screen: "screen_1",
      backCamera: "backCamera_1",
      frontCamera: "frontCamera_1",
      img: "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
      desc: "iPhone X features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      id: "sp_2",
      name: "Note 7",
      price: 2000,
      screen: "screen_2",
      backCamera: "backCamera_2",
      frontCamera: "frontCamera_2",
      img: "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
      desc: "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
    },
    {
      id: "sp_3",
      name: "Vivo",
      price: 1200,
      screen: "screen_3",
      backCamera: "backCamera_3",
      frontCamera: "frontCamera_3",
      img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
      desc: "A young global smartphone brand focusing on introducing perfect sound quality",
    },
    {
      id: "sp_4",
      name: "Blacberry",
      price: 1500,
      screen: "screen_4",
      backCamera: "backCamera_4",
      frontCamera: "frontCamera_4",
      img: "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
      desc: "BlackBerry is a line of smartphones, tablets, and services originally designed",
    },
  ];
  render() {
    return (
      <div>
        <h1 className="text-center">Bài tập giỏ hàng</h1>
        <h4
          data-toggle="modal"
          data-target="#modelId"
          className="text-center text-danger"
        >
          Giỏ hàng
        </h4>

        <ProductList
          setSelectedProduct={this.setSelectedProduct}
          addToCart={this.addToCart}
          products={this.products}
        />

        <Cart
          cart={this.state.cart}
          handlePayment={this.handlePayment}
          increaseQuantity={this.increaseQuantity}
          decreaseQuantity={this.decreaseQuantity}
          deleteFromCart={this.deleteFromCart}
        />
        {this.state.selectedProduct && (
          <Detail selectedProduct={this.state.selectedProduct} />
        )}
      </div>
    );
  }
}

export default Home;
