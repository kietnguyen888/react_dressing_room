import React, { Component } from "react";
import TodoItem from "./TodoItem";
class Todo extends Component {
  constructor(props) {
    super(props);
    console.log("constructor");
    this.state = {
      counter: 1,
      counter2: 1,
      todoList: [
        {
          id: 1,
          name: "learn Reactjs",
        },
        {
          id: 2,
          name: "learn NodeJs",
        },
      ],
    };
  }
  //   componentWillMount() {
  //     console.log("willMount");
  //   } react ver 17 remove willmount
  handleCreateCounter = () => {
    this.setState({ counter: this.state.counter + 1 });
  };

  handleCreateCounter2 = () => {
    this.setState({ counter2: this.state.counter2 + 1 });
  };
  render() {
    console.log("todo render");
    return (
      <div>
        <h1>To do list</h1>
        <button onClick={this.handleCreateCounter}>Increase counter</button>
        <button onClick={this.handleCreateCounter2}>Increase counter2</button>

        <h2>Counter1: {this.state.counter}</h2>
        <h2>Counter2: {this.state.counter2}</h2>
        {this.state.todoList.map((item) => {
          return (
            <TodoItem
              counter2={this.state.counter2}
              key={item.id}
              item={item}
            />
          );
        })}
      </div>
    );
  }
  fetchTask() {
    //ko goi fetch task trong render vi se tao ra vong lap vo tan
    //vi khi call api fetch lai state se render lai
    console.log("call api");
  }
  //bo code ma ta muon chay luc dau vao didmount
  componentDidMount() {
    console.log("todo mounted");
    this.fetchTask();
    //cap nhat task
    setTimeout(() => {
      const cloneTodoList = [...this.state.todoList];
      cloneTodoList[1] = { ...cloneTodoList[1], name: "take a shower" };
      this.setState({ todoList: cloneTodoList });
    }, 3000);
  }

  //----------------update life cycle----------------------------
  shouldComponentUpdate(nextProps, nextState) {
    console.log("shoud update", nextState);
    return true;
  }
  //   componentWillUpdate() {
  //     console.log("will update");
  //   }react ver 17 remove willUpdate
  componentDidUpdate(prevProps, prevState) {
    //Xu li logic sau khi update
    console.log("old counter", prevState.counter);
    console.log("New counter", this.state.counter);
  }

  //clean up logic: socketio, event Listener
  componentWillUnmount() {}
}

export default Todo;
