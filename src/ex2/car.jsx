import React, { Component } from "react";
import redCar from "../asset/image/red-car.jpg";
import blackCar from "../asset/image/black-car.jpg";
import silverCar from "../asset/image/silver-car.jpg";

class Car extends Component {
  state = {
    imgSource: redCar,
  };
  handleChangeColor = (image) => {
    return () => {
      this.setState({
        imgSource: image,
      });
    };
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-8">
            <img className="w-100" src={this.state.imgSource} alt="" />
          </div>
          <div className="col-4">
            <button
              onClick={this.handleChangeColor(redCar)}
              className="btn btn-danger mx-3"
            >
              Red
            </button>
            <button onClick={this.handleChangeColor(silverCar)} className="btn btn-secondary mx-3">Silver</button>
            <button onClick={this.handleChangeColor(blackCar)} className="btn btn-dark mx-3">Black</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Car;
