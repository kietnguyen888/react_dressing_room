// rcc enter
import React, { Component } from "react";

class Databinding extends Component {
  fullName = "anh kiet";
  email = "kiet@gmail.com";
  state = {
    isLogin: false,
  };

  renderMessage = () => {
    // return <p>HelloWorld</p>;
    return "Hello World";
  };

  showMessage = () => {
    alert("hello World");
  };
  //closure : tập hợp fucntion và environgment khoi tạo fucntion đó, cho phep ta truy cap vao bien cua fuction cha (outer function)
  showMessageWithParam = (message) => {
    return () => {
      alert(message);
    };
  };

  showMessageWithThis = () => {
    alert(`hello ${this.fullName}`);
  };

  //-------------NOT WRITE LIKE THIS--------------this =>undefined (user strict of react)
  //   showMessageWithThis() {
  //     alert(`hello ${this.fullName}`);
  //   }

  //funtion check dieu kien, return giao dien tuong ung
  renderHeader = () => {
    if (this.state.isLogin) {
      return <a className="text-white">Hello kiet</a>;
    }
    return (  
      <>
        <a className="mx-4 text-white">Sign in</a>
        <a className="mx-4 text-white">Sign up</a>
      </>
    );
  };

  handleLogin = () => {
    this.setState({
      isLogin: true,
    });
  };

  handleLogout = () => {
    this.setState({
      isLogin: false,
    });
  };
  render() {
    return (
      <div>
        <h1>------Data binding------</h1>
        <h3>FullName: {this.fullName}</h3>
        <h3>Email: {this.email}</h3>
        <h3>Age: {14 + 2}</h3>
        {/* {this.renderMessage()} */}
        <p>{this.renderMessage()}</p>
        <h1>-------------Event Bunding--------------</h1>
        <button onClick={this.showMessage}>Show Message</button>
        <button onClick={this.showMessageWithParam("hello there")}>
          Show Message with Param
        </button>

        <button onMouseEnter={this.showMessageWithThis}>
          show Message With This
        </button>

        <h1>------------Lam viec voi lenh dieu kien </h1>
        <h4>Dung dieu kien de an hien thanh phan tren giao dien</h4>

        <div className="bg-dark p-3">{this.renderHeader()}</div>
        <div>
          {this.state.isLogin ? (
            <button onClick={this.handleLogout}>Dang xuat</button>
          ) : (
            <button onClick={this.handleLogin}>Dang nhap ngay</button>
          )}
        </div>
      </div>
    );
  }
}

export default Databinding;
