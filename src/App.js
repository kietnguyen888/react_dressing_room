import './App.css';
import Todo from './demoLifeCycle/todo';
import Home from './dressingRoom/Home';

// import Databinding from './component/databinding';
// import Car from './ex2/car';
// import Movie from './component/ex3/movie';
// import StudentList from './demoProps/studentLIst';
// import Home from './shoppingcart/home';
function App() {
  return (
    <div className="App">
     <Home/>
    </div>
  );
}

export default App;
