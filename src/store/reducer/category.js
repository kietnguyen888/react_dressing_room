//khai bao gia tri ban dau reducer quan li

const initialState = {
  categoryList: [
    { tabName: "tabTopClothes", showName: "Áo", type: "topclothes" },
    { tabName: "tabBotClothes", showName: "Quần", type: "botclothes" },
    { tabName: "tabShoes", showName: "Giày dép", type: "shoes" },
    { tabName: "tabHandBags", showName: "Túi xách", type: "handbags" },
    { tabName: "tabNecklaces", showName: "Dây chuyền", type: "necklaces" },
    // { tabName: 'tabModels', showName: 'Người mẫu', type: 'models' },
    { tabName: "tabHairStyle", showName: "Kiểu tóc", type: "hairstyle" },
    { tabName: "tabBackground", showName: "Nền", type: "background" },
  ],

  selectedCategory: "topclothes",
};

//khoi tao reducer: la 1 function, co 2 tham so la state(gia tri dang quan li) va action (yeu cau tu component)
//nhan action, chinh sua state theo action =>return state moi sau khi chinh sua
//shallow comparation:so sanh nông(chi so sanh obj ben ngoai ko so sanh ben trong(chi so sanh vung nho obj))
const reducer = (state = initialState, action) => {
  //phan biet action = type
  switch (action.type) {
    case "SET_SELECTED_CATEGORY":
      state.selectedCategory = action.payload;
      return {...state};
    default:
      return state;
  }
};

export default reducer;
