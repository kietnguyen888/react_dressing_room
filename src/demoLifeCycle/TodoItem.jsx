import React, { PureComponent } from "react";

class TodoItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isSelected: false,
    };
  }

  //   shouldComponentUpdate(nextProps, nextState) {
  //     if (
  //       this.props.counter2 !== nextProps.counter2 ||
  //       this.state.isSelected !== nextState.isSelected
  //     ) {
  //       return true;
  //     }
  //     return false;
  //   }

  handleSelect = () => {
    this.setState({ isSelected: !this.state.isSelected });
  };

  render() {
    console.log("Item render");
    return (
      <div
        className={this.state.isSelected ? "bg-success" : "bg-danger"}
        onClick={this.handleSelect}
      >
        <h2>{this.props.item.name}</h2>
      </div>
    );
  }
}

export default TodoItem;
