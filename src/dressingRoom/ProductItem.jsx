import React, { Component } from "react";
import { connect } from "react-redux";

class ProductItem extends Component {
  handleSeLect = () => {
    const { type, imgSrc_png: img } = this.props.prod;
    this.props.dispatch({
      type: "SELECT_PRODUCT",
      payload: { img, type },
    });
  };
  render() {
    const { imgSrc_jpg, name } = this.props.prod;
    return (
      <div className="card mb-3">
        <img src={imgSrc_jpg} />
        <div className="card-body">
          <p className="lead">{name}</p>
          <button onClick={this.handleSeLect} className="btn btn-success">
            Thử
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(ProductItem);
