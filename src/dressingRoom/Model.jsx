import React, { Component } from "react";
import { connect } from "react-redux";
import "./model.css";
class Model extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${
            this.props.selectedProducts.background ||
            "./assets/img/background/background_998.jpg"
          })`,
        }}
        className="contain"
      >
        <div
          style={{
            backgroundImage: "url(./assets/img/allbody/bodynew.png)",
          }}
          className="body"
        />
        <div
          style={{ backgroundImage: "url(./assets/img/model/1000new.png)" }}
          className="model"
        />
        <div
          style={{
            backgroundImage: "url(./assets/img/allbody/bikini_branew.png)",
          }}
          className="bikinitop"
        />

        <div
          style={{
            backgroundImage: "url(./assets/img/allbody/bikini_pantsnew.png)",
          }}
          className="bikinibottom"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/feet_high_leftnew.png)",
          }}
          className="feetleft"
        />
        <div
          style={{
            backgroundImage: "url(./assets/img/allbody/feet_high_rightnew.png)",
          }}
          className="feetright"
        />
        {/* div hien ao ngoai */}
        <div
          style={{
            backgroundImage: `url(${this.props.selectedProducts.topclothes})`,
            backgroundSize: "cover",
          }}
          className="bikinitop"
        />
        <div
          style={{
            backgroundImage: `url(${this.props.selectedProducts.botclothes})`,
            backgroundSize: "cover",
          }}
          className="bikinibottom"
        />
        <div
          style={{
            backgroundImage: `url(${this.props.selectedProducts.shoes})`,
            backgroundSize: "cover",
          }}
          className="bikinibottom"
        />
         <div
          style={{
            backgroundImage: `url(${this.props.selectedProducts.handbags})`,
            backgroundSize: "cover",
          }}
          className="bikinibottom"
        />
         <div
          style={{
            backgroundImage: `url(${this.props.selectedProducts.necklaces})`,
            backgroundSize: "cover",
          }}
          className="bikinibottom"
        />
        <div
          style={{
            backgroundImage: `url(${this.props.selectedProducts.hairstyle})`,
            backgroundSize: "cover",
          }}
          className="hairStyle"
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedProducts: state.product.selectedProducts,
  };
};
export default connect(mapStateToProps)(Model);
