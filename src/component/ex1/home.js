
import React from "react";
import SideBar from "./sidebar";
import Content from "./content";
import Footer from "./footer";
import Header from "./header";
import "./home.css"
class Home extends React.Component {
  render() {
    return (
      <div className="home">
        <Header />
        <div className="container">
          <SideBar />
          <Content />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
