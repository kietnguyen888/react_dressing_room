import React, { Component } from "react";
import { connect } from "react-redux";
class Category extends Component {
  handleSelectCategory = (categoryType) => {
    this.props.dispatch({
      type: "SET_SELECTED_CATEGORY",
      payload: categoryType,
    });
  };
  renderCategories = () => {
    return this.props.categories.map((item) => {
      return (
        <button
          onClick={() => this.handleSelectCategory(item.type)}
          key={item.type}
          className={
            item.type === this.props.selectedCategory
              ? "btn btn-success"
              : "btn btn-secondary"
          }
        >
          {item.showName}
        </button>
      );
    });
  };
  render() {
    // this.props.categories
    return (
      <div className="btn-group container-fluid mb-4">
        {this.renderCategories()}
      </div>
    );
  }
}
//state la object tren store
//function lay du lieu tu store chuyen thanh props cua component
const mapStateToProps = (state) => {
  return {
    categories: state.category.categoryList,
    selectedCategory: state.category.selectedCategory,
  };
};
export default connect(mapStateToProps)(Category);
